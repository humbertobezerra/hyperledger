# install go sdk
go get -u github.com/hyperledger/fabric-sdk-go
export GOROOT=/usr/local/go
export PATH=$PATH:$GOROOT/bin
cd $GOPATH/src/github.com/hyperledger/fabric-sdk-go/
make depend-install
make

# References
https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html
https://hyperledger-fabric-ca.readthedocs.io/en/latest/servercli.html
https://hyperledger-fabric-ca.readthedocs.io/en/latest/clientcli.html
https://www.youtube.com/watch?v=DKuGU5CYV_E
https://ksachdeva.github.io/2017/07/23/bootstrapping-hyperledger-fabric-nw-4/
https://linuxctl.com/2017/08/bootstrapping-hyperledger-fabric-1.0/
https://github.com/LedgerDomain/FabricWebApp
https://gendal.me/2017/07/20/what-slack-can-teach-us-about-privacy-in-enterprise-blockchains/
https://www.imf.org/external/pubs/ft/sdn/2016/sdn1603.pdf
https://hyperledger.github.io/composer/jsdoc/module-composer-runtime.html
https://kafka.apache.org/intro.html
https://zookeeper.apache.org
https://github.com/LedgerDomain/FabricWebApp/blob/master/docker-compose.yaml
