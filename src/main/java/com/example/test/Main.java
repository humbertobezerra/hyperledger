package com.example.test;

import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.Orderer;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.TransactionProposalRequest;
import org.hyperledger.fabric.sdk.ChaincodeID;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric.sdk.exception.CryptoException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.TransactionException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import java.security.PrivateKey;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) throws CryptoException,
                                                              InvalidArgumentException,
                                                              TransactionException,
                                                              ProposalException,
                                                              InterruptedException,
                                                              ExecutionException {
        HFClient client = HFClient.createNewInstance();
        client.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());

        // create user and connect to the network
        String filepath_pk = "producerApp/certs/02383c00cb8f1bddc8f2edcd8ee7c664241e13624879421de51c863dbc2c1bc0_sk";
        PrivateKey privateKey = KeyUtils.readPrivateKey(filepath_pk);

        String filepath_cert = "producerApp/certs/Admin@Asus.com-cert.pem";
        String cert = KeyUtils.readPemCertificate(filepath_cert);
        TestUser testUser = new TestUser(privateKey, cert);
        client.setUserContext(testUser);

        // setup pear and channel then initialize the network
        Peer asusPeer = client.newPeer("Asus", "grpc://localhost:7051");
        Orderer orderer = client.newOrderer("orderer", "grpc://localhost:7050");
        Channel channel0 = client.newChannel("asus");

        channel0.addOrderer(orderer);
        channel0.addPeer(asusPeer);

        channel0.initialize();

        // create and send transaction
        final TransactionProposalRequest proposalRequest = client.newTransactionProposalRequest();

        final ChaincodeID chaincodeID = ChaincodeID.newBuilder()
                .setName("pcxchg")
                .setVersion("1.0")
                .setPath("chaincode/pcxchg")
                .build();

        // chaincode name
        proposalRequest.setChaincodeID(chaincodeID);
        // chaincode function to execute
        proposalRequest.setFcn("createPC");
        // timeout
        proposalRequest.setProposalWaitTime(60000);
        // arguments for chaincode function
        String[] params = { "Asus003", "foo", "bar" };
        proposalRequest.setArgs(params);

        // Sending transaction proposal
        final Collection<ProposalResponse> responses = channel0.sendTransactionProposal(proposalRequest);

        System.out.println("sending transaction");

        CompletableFuture<BlockEvent.TransactionEvent> txFuture = channel0.sendTransaction(responses, client.getUserContext());

        BlockEvent.TransactionEvent event = txFuture.get();
    }
}
