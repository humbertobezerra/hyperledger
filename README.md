# Step 1
docker exec cli.Amazon bash -c "chmod a+x ./scripts/init-amazon-cli.sh && ./scripts/init-amazon-cli.sh"

# Step 2
chmod a+x ./scripts/init-network.sh && ./scripts/init-network.sh

# Queries
docker exec cli.Amazon bash -c "peer chaincode query -C asus -n producer -v 0 -c '{\"Args\":[\"queryStock\"]}'"
docker exec cli.Amazon bash -c "peer chaincode query -C hp -n producer -v 0 -c '{\"Args\":[\"queryStock\"]}'"
docker exec cli.Amazon bash -c "peer chaincode query -C dell -n producer -v 0 -c '{\"Args\":[\"queryStock\"]}'"

# Invoke some commands
docker exec cli.Amazon bash -c "peer chaincode invoke -C dell -n market -v 0 -c '{\"Args\":[\"buyPC\", \"Dell004\", \"producer\"]}'"

docker exec cli.Amazon bash -c "peer chaincode query -C asus -n producer -v 0 -c '{\"Args\":[\"queryDetail\", \"Asus003\"]}'"
docker exec cli.Amazon bash -c "peer chaincode invoke -C asus -n market -v 0 -c '{\"Args\":[\"buyPC\", \"Asus003\", \"producer\"]}'"
docker exec cli.Amazon bash -c "peer chaincode query -C asus -n producer -v 0 -c '{\"Args\":[\"queryDetail\", \"Asus003\"]}'"

docker exec cli.Amazon bash -c "peer chaincode query -C hp -n producer -v 0 -c '{\"Args\":[\"queryDetail\", \"HP001\"]}'"
docker exec cli.Amazon bash -c "peer chaincode invoke -C hp -n market -v 0 -c '{\"Args\":[\"buyPC\", \"HP001\", \"producer\"]}'"
docker exec cli.Amazon bash -c "peer chaincode query -C hp -n producer -v 0 -c '{\"Args\":[\"queryDetail\", \"HP001\"]}'"

docker exec cli.Amazon bash -c "peer chaincode invoke -C dell -n market -v 0 -c '{\"Args\":[\"buyPC\", \"Dell002\",\"producer\"]}'
docker exec cli.Amazon bash -c "peer chaincode query -C dell -n producer -v 0 -c '{\"Args\":[\"queryDetail\", \"Dell002\"]}'"

# Remove all unused images
docker image rm -f $(docker image ls -f=reference='dev-peer*' -q)

# CouchDB
http://localhost:5984/_utils/#_all_dbs
