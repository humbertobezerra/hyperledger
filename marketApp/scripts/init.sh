cp ../crypto-config/peerOrganizations/Amazon.com/users/Admin@Amazon.com/msp/admincerts/Admin@Amazon.com-cert.pem ./certs
cp ../crypto-config/peerOrganizations/Amazon.com/users/Admin@Amazon.com/msp/keystore/* ./certs
awk '{printf "%s\\n", $0}' ./certs/Admin@Amazon.com-cert.pem > ./certs/AmazonAdminFormatted.pem
#copy and past content of Formatted.pem file in Admin file
cp ./certs/c32556b89cd639c7219ed64567b9428fc861afd6bbecc0fe71e6ec113d400d71_sk ~/.hfc-key-store/c32556b89cd639c7219ed64567b9428fc861afd6bbecc0fe71e6ec113d400d7-priv
